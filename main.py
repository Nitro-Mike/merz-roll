import discord
import os
import traceback
from random import randrange

client = discord.Client()
validCommands = [
    "roll", "r",
    "magic",
    "help",
    "test"
]
eightBallResponses = [
    "That’s worse than shredding your balls on a cheese grater."
    "Don’t count on it.",
    "It is certain.",
    "It is decidedly so.",
    "Most likely.",
    "My reply is no.",
    "My sources say no.",
    "Outlook not so good.",
    "Outlook good.",
    "Reply hazy, try again.",
    "Signs point to yes.",
    "Very doubtful.",
    "Without a doubt.",
    "Yes.",
    "Yes – definitely.",
    "You may rely on it."]
randomEightBallColors = [
    '0x1abc9c',
    '0x11806a',
    '0x2ecc71',
    '0x1f8b4c',
    '0x3498db',
    '0x206694',
    '0x9b59b6',
    '0x71368a',
    '0xe91e63',
    '0xad1457',
    '0xf1c40f',
    '0xc27c0e',
    '0xe67e22',
    '0xa84300',
    '0xe74c3c',
    '0x992d22',
    '0x95a5a6',
    '0x607d8b',
    '0x979c9f',
    '0x546e7a',
    '0x7289da',
    '0x99aab5']
randomErrorResponses = [
    "Shitz Fukked",
    "I Borked",
    "Errz",
    "Christopher Mersman sucks at programming...",
    "So sorry I can't process your request because I is dumbz",
    "Explosions happened and whatever you wanted didn't work",
    "I'm busy... jk I've failed you"]
randomFailureResponses = [
    "You are the god of being bad.",
    "Just... stop... please",
    "Suck. Less.",
    "*sigh*",
    "Do you always roll this bad?",
    "I hate to say it, but that's a bad roll.",
    "Better luck next roll...",
    "You're like the nutsack of a mouse being eaten by a drowning lion."]
randomSuccessResponses = [
    "Good news everyone!",
    "GOOD SCOTT GREAT SHOT",
    "YOU ARE A GOD",
    "HAPPY HOLIDAYS!",
    "I'd rather be lucky than good.",
    "You've brought her to her milk.",
    "Sure as God's got sandals.",
    "You rolled about as good as Christopher Mersman looks.",
    "$$$ Get laid, get paid $$$"]

def parseMessage(message):
    numbers = [] 
    randomMax = 20
    params = message.split()
    dice = "1d20"

    if len(params) > 1:
        dice = params[1]

    if "d" in dice:
        numOfDi = int(dice.split("d",1)[0])
        randomMax = int(dice.split("d",1)[1])
        for i in range(0 ,numOfDi):
            numbers.append(randrange(randomMax) + 1) 

    return numbers, randomMax

def formatResponse(index, number, best, size):
    suffix = ""
    if size == 1:  # only enable fun responses on 1 die roll
        # print("index %d number %d best %d size %d" % (index, number, best, size))
        if number == 1 and best > 5:
            suffix = " - " + randomFailureResponses[randrange(0,len(randomFailureResponses))]
        elif number == best and best > 5:
            suffix = " - " + randomSuccessResponses[randrange(0,len(randomSuccessResponses))]

    if size == 1:
        return "**[ "+str(number)+"**/"+str(best)+" **]**" + suffix
    return "roll #"+str(index+1)+": **[ "+str(number)+"**/"+str(best)+" **]**" + suffix

def getColor(number,best):
    color = 0x4BB543  # success
    if float(number) < .62 * best:  # warning
        color = 0xEED202
    if float(number) < .30 * best:  # failure
        color = 0xFF0000
    return color

def buildDescriptionAndColor(message, numbers, best):
    rolledTotal = 0
    bestTotal = best * len(numbers)
    fullResponse = "**" + message.author.display_name + "'s roll!**\n\n"

    if len(numbers) == 1:
        rolledTotal = numbers[0]
        fullResponse += formatResponse(0, numbers[0], best, len(numbers))
    else:    
        for i in range(0, len(numbers)):
            rolledTotal += numbers[i]
            fullResponse += "> " + formatResponse(i, numbers[i], best, len(numbers)) + "\n"
        fullResponse += "\n**Total: [ "+ str(rolledTotal) +"**/" + str(bestTotal) + " **]**"

    color = getColor(rolledTotal, bestTotal)
    return fullResponse, color

def getHelpMessage():
    helpMessage = "Valid merzbot commands include:"
    for command in validCommands:
        helpMessage += "\n> **!%s**" % command
    return helpMessage

def magicEightBallResponse():
    eightBallResponses = [randrange(len(eightBallResponses))]
    color = [randrange(len(randomEightBallColors))]
    return eightBallResponses, color

@client.event
async def on_ready():
    print('We have logged in as {0.user}'.format(client))

@client.event
async def on_message(message):
    if message.author == client.user:
        return
    
    try:
        ##### !test command #####
        if message.content.startswith('!test') or message.content.startswith("!help"):
            await message.channel.send("", embed=discord.Embed(description=getHelpMessage(), color=0xFFFFFF))

        ##### !magic command #####
        if message.content.startswith('!magic'):
            await message.channel.send("", embed=discord.Embed(description=magicEightBallResponse(), color=color))

        ##### !roll commands #####
        if message.content.startswith('!roll') or message.content.startswith('!r'):
            numbers, best = parseMessage(message.content)
            desc, color = buildDescriptionAndColor(message, numbers, best)
            await message.channel.send("", embed=discord.Embed(description=desc, color=color))

    except Exception:
        traceback.print_exc()
        await message.channel.send(randomErrorResponses[randrange(len(randomErrorResponses))])

client.run(os.getenv('DISCORD_KEY'))
